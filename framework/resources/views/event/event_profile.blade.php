@extends('layouts.master')

@section('ep_css')
	<style type="text/css">
		.nav-tabs .nav-link.active,
		.nav-tabs .nav-item.show .nav-link {
		  color: white;
		  background-color: #ff9649;
		}

		.nav-tabs .nav-link {
		  border: 0;
		  color: black;
		}

		h3{
			color: #ff9649;
		}

		#user{
			margin-left: 30px;
		}

		#admin{
			margin-left: 80px;
			color: orange;
		}
	</style>
@stop

@section('content')
	<div class="container" style="margin-top: 40px;">
		<button class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH PENCARIAN</b></span></button>
		<br>
		<br>
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  </ol>
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img class="d-block w-100" src="assets/master/img/home/slider/001.jpg" alt="First slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="assets/master/img/home/slider/002.jpg" alt="Second slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="assets/master/img/home/slider/003.jpg" alt="Third slide">
		    </div>
		  </div>
		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>

		<br>

		<div class="jumbotron" style="padding: 10px;">
			<div class="container">
				<p style="color: orange; margin-left: 40px; margin-bottom: -1px;"><i>Highlight</i></p>
				<div class="row">
					<div class="col-md-8">
						<h2 style="margin-left: 40px;">WORLD OF GHIBLI JAKARTA EXHIBITION</h2>
					</div>
					<div class="col-md-4">
						<p class="btn" style="background-color: #ff9649; color: white;">KIDS</p>
						<p class="btn" style="background-color: #ff9649; color: white;">FASHION</p>
					</div>
				</div>
				<hr style="background-color: orange; height: 1px;">
				<div class="row">
					<div class="col-md-3">
						<p><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; 17 - 20 AGUSTUS 2017</p>
						<p><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; PLAZA INDONESIA</p>
						<p><img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; RP 1.500.000 - RP 2.500.000</p>
						<p><img src="" width="20" height="20">&nbsp; 09.00 - 22.00</p>
						<br>
						<p style="margin-bottom: -3px;">PENGELOLA / EVENT ORGANAIZER</p>
						<a href="" style="color: orange;">PT. LININDO JAKARTA</a>
					</div>
					<div class="col-md-3">
						<img src="assets/master/img/search-engine/sample.png" width="200" height="200" style="margin-top: 10px;">
					</div>
					<div class="col-md-6">
						<p><b>DESKRIPSI ACARA</b></p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur.</p>
						<p style="margin-bottom: -3px;"><b>KONDISI DAN ISYARAT</b></p>
						<a href="" style="color: orange;"><i>lihat</i></a>
					</div>
				</div>
			</div>
		</div>

		<div class="jumbotron" style="padding: 10px;">
		</div>

		<div class="jumbotron" style="padding: 10px;">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="aboutevent-tab" data-toggle="tab" href="#aboutevent" role="tab" aria-controls="aboutevent" aria-selected="true">TENTANG EVENT</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="facility-tab" data-toggle="tab" href="#facility" role="tab" aria-controls="facility" aria-selected="false">FASILITAS</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="playrole-tab" data-toggle="tab" href="#playrole" role="tab" aria-controls="playrole" aria-selected="false">ATURAN MAIN</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="cancelrole-tab" data-toggle="tab" href="#cancelrole" role="tab" aria-controls="cancelrole" aria-selected="true">ATURAN PEMBATALAN</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">REVIEW PENYELENGGARA</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="question" aria-selected="false">PERTANYAAN</a>
			  </li>
			</ul>
			<hr style="margin-top: -2px; background-color: #ff9649; height: 3px;">
			<br>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane fade show active" id="aboutevent" role="tabpanel" aria-labelledby="aboutevent-tab">
			  	<h3>DESKRIPSI EVENT <b>INDEPENDENCE DAY</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			  	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			  	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			  	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			  	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			  </div>
			  <div class="tab-pane fade" id="facility" role="tabpanel" aria-labelledby="facility-tab">
			  	<h3>FASILITAS <b>TERSEDIA</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	<p>1. Electricity 2 Ampere</p>
			  	<p>2. Free Parking</p>
			  	<p>3. WiFi</p>
			  </div>
			  <div class="tab-pane fade" id="playrole" role="tabpanel" aria-labelledby="playrole-tab">
			  	<h3>ATURAN <b>MAIN</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	<p>Check in anytime after 9 AM.</p>
			  	<p>Check out anytime before 10 AM.</p>
			  </div>
			  <div class="tab-pane fade" id="cancelrole" role="tabpanel" aria-labelledby="cancelrole-tab">
			  	<h3>ATURAN <b>PEMBATALAN</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	<p>Cancel up to 5 days before check in and get full refund(minus service fees). Cancel within 5 days of your trip and the first night is non-refundable, but 50% of the cost for remaining night will be refunded. Service fees are refunded when cancellation happens before check in and within 48 hours of booking.</p>
			  </div>
			  <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
			  	<h6 style="color: #ff9649">DISELENGGARAI OLEH</h6>
			  	<h3>PT. LININDO JAKARTA</h3>
			  	<p>Ruko Alam Sutra blok A1 no.13 Alam Sutera. Tangerang. | Bergabung sejak <b>4 Juli 2019</b></p>
			  	<hr style="background-color: #ff9649;">
			  	<p><b>Pt. Linindo Jakarta adalah Superhost</b> | Superhost adalah penyelenggara yang telah mendapatkan review baik dalam jumlah yang banyak dengan pengalaman lebih dari 1 tahun dan lebih dari 5 pameran.</p>
			  	<hr style="background-color: #ff9649;">
			  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			  	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			  	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			  	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			  	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			  </div>
			  <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="question-tab">
			  	<h3>PERTANYAAN MENGENAI EVENT <b>INDEPENDENCE DAY</b></h3>

			  	<br>
			  	<br>

			  	<div id="user">
			  		<p style="color: grey;"><img src="assets/master/img/event/default-img.png" width="30" height="30"> ABAH GANI</p>
				  	<hr style="background-color: #ff9649;">
				  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				  	tempor incididunt ut labore et dolore magna aliqua.</p>
			  	</div>

			  	<br>

			  	<div id="admin">
			  		<p><img src="assets/master/img/event/default-img.png" width="30" height="30"> PT. LININDO JAKARTA</p>
				  	<hr style="background-color: #ff9649;">
				  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				  	tempor incididunt ut labore et dolore magna aliqua.</p>
			  	</div>

			  	<br>

			  	<div id="user">
			  		<p style="color: grey;"><img src="assets/master/img/event/default-img.png" width="30" height="30"> ABAH GANI</p>
				  	<hr style="background-color: #ff9649;">
				  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				  	tempor incididunt ut labore et dolore magna aliqua.</p>
			  	</div>

			  	<br>

			  	<div id="admin">
			  		<p><img src="assets/master/img/event/default-img.png" width="30" height="30"> PT. LININDO JAKARTA</p>
				  	<hr style="background-color: #ff9649;">
				  	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				  	tempor incididunt ut labore et dolore magna aliqua.</p>
			  	</div>
			  </div>
			</div>
		</div>
	</div>
@stop