@extends('layouts.master')

@section('title', 'Register Profil Pengguna')

@section('rp_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .in, .out {
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-duration: 500ms !important;
    }

    label{
      float: left;
    }

    p{
      text-align: left;
      font-style: italic;
    }

    a{
      float: left;
    }

    .font{
        font-weight:300;
        font-size:40px;
    }
    #social{
        display: none;
        padding:20px;
    }

    #personal{
        display:none;
        padding:20px;
    }

    #account{
        display: block;
        padding:20px;
    }

    .text.container{

        width:50%;

    }


    #logo{
        border:2px solid white;
        border-radius: 10px;
        color:white;
        padding:15px;
    }
    #logo:hover{
        box-shadow: 0px 0px 5px 5px white;
    }
  </style>
@endsection

@section('content')
<body style="background-color: #e9e9e9;">
  <div class="ui centered  grid container">

    <div class="row"></div>
    <div class="row"></div>
    <div class="row"></div>


    <div class="ui text container">


      <div class="three ui buttons center aligned grid container" style="margin:20px;">


        <div class=" ui  big orange button" id="accountS">

          <div class="content " style="font-size:12px;color: white;">PROFIL PENGGUNA</div>
        </div>


        <button class=" ui  big disabled blue button" id="socialP">
             <div class="content" style="font-size:12px;color:white;">PROFIL PERUSAHAAN</div>
             </button>


        <button class=" disabled ui big green button" id="details">

             <div class="content" style="font-size:12px;color:white;">PROFIL BRAND</div>
             </button>
      </div>


      <div class="row"></div>
      <div class="row"></div>


      <div id="account">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #fff; border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color: orange;">Lengkapi Profil Anda!</h1>
          </div>

          <br>

          <form class="ui form">
            <div class="field">
                <img src="assets/master/img/user/user-img.png" width="100px" height="100px">
                <br>
                <button style="margin-top: 10px;" class="ui  inverted orange  medium button" onclick="document.getElementById('getUserImg').click()">Unggah Foto</button>
                <input type='file' id="getUserImg" style="display:none" required="required">
            </div>

            <br>

            <div class="field">
                <label>Nama Lengkap</label>
                <input type="text" placeholder="-" name="name" id="name" required="required">
            </div>

            <div class="field">
                <label>Email</label>
                <input type="email" placeholder="mail@mail.com" name="email" id="email" required="required">
            </div>

            <div class="field">
                <label>Kata Sandi</label>
                <input type="password" placeholder="-" name="password" id="password" required="required">
            </div>

            <div class="field">
                <label>Konfirmasi Kata sandi</label>
                <input type="password" placeholder="-" name="password2" id="password2" required="required">
            </div>

            <div class="field">
                <label>No Telepon</label>
                <input type="text" placeholder="08389866****" name="telpnumb" id="telpnumb" required="required">
            </div>

            <div class="field">
                <label>Alamat Lengkap</label>
                <textarea placeholder="-" name="address" id="address" cols="40" rows="4" style="resize: none;"></textarea>
            </div>

            <div class="field">
                <label>Kode Pos</label>
                <input type="number" placeholder="-" name="poscode" id="poscode" required="required">
            </div>

            <div class="one ui buttons">
              <button class="ui  inverted orange  medium button next1 ">Next</button>
            </div>

          </form>


        </div>

      </div>


      <div id="social">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:rgb(50,153,153); font-size: 35px;">Lengkapi Profil Usaha Anda!</h1>
          </div>

          <p>Pastikan anda terhubung dengan para tenan dari pasar lokal dan internasional secara cepat!</p>

          <br>

          <form class="ui form">
            <div class="field">
              <label>Nama Penyelenggara</label>
                <input type="text" placeholder="Misal: TUKMU" name="organizername" id="organizername">
            </div>

            <div class="field">
              <label>Nama Perusahaan</label>
                <input type="text" placeholder="Misal: Pt. Tukmu" name="companyname" id="companyname">
            </div>

            <div class="field">
              <label>No Telepon</label>
                <input type="text" placeholder="08389866****" name="companytelp" id="companytelp">
            </div>

            <div class="field">
              <label>Deskripsi Perusahaan</label>
                <textarea placeholder="Contoh: Peralatan masak / Aksesoris handphone / Makanan siap saji" name="companydesc" id="companydesc" cols="40" rows="4" style="resize: none;"></textarea>
            </div>

            <div class="field">
              <label>Foto Profil / Logo Perusahaan</label>
              <br>
              <br>
              <img src="assets/master/img/user/add-img.png" align="left" class="ui  inverted blue  medium button" onclick="document.getElementById('getCompanyImg').click()" width="100px" height="90px">
              <input type='file' id="getCompanyImg" style="display:none" required="required">
            </div>

            <br>
            <br>
            <br>
            <br>
            <hr style="background-color: #329999;">
            <br>

            <div class="field">
              <h4>PENANGGUNG JAWAB</h4>
            </div>

            <p style="text-align: center;">Anda bisa memasukkan sampai 5 nama penanggung jawab</p>

            <br>

            <div class="field">
              <label>Nama Penanggung Jawab</label>
                <input type="text" placeholder="-" name="coordinatorname" id="coordinatorname">
            </div>

            <div class="field">
              <label>No Telepon Selular</label>
                <input type="text" placeholder="-" name="coordinatortelp" id="coordinatortelp">
            </div>

            <div class="field">
              <label>Email</label>
                <input type="text" placeholder="email@email.com" name="coordinatoremail" id="coordinatoremail">
            </div>

            <div class="two ui buttons">
              <button class="ui  inverted blue  medium button prev1">Previous</button>
              <button class="ui  inverted blue  medium button next2">Next</button>
            </div>

          </form>


        </div>

      </div>


      <div id="personal">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:green; font-size: 35px;">Lengkapi Profil Usaha Anda!</h1>
          </div>

          <p>Pastikan Anda terhubung dengan pasar tenan dari pasar lokal dan internasional secara cepat!</p>

          <br>

          <a href="#" style="color: green;"><u><i>Terms & Condition</i></u></a>
          <p style="margin-top: 30px; margin-bottom: 1px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p><a href="#" style="color: green;"><b>Read More</b></a>

          <br>
          <br>

          <form class="ui form">
            <div class="field">
              <label>Nama Brand &nbsp; &nbsp; &nbsp;</label><p>Anda bisa memasukkan sampai 5 nama brand</p>
                <input type="text" placeholder="-" name="brandname" id="brandname">
            </div>

            <div class="field">
              <label>Kategori Brand</label>
                <select name="brandcategory" id="brandcategory">
                  <option>Pilih Kategori</option>
                  <option value="makanan">Makananan</option>
                </select>
            </div>

            <div class="field">
              <label>No Telepon</label>
                <input type="text" placeholder="-" name="brandtelp" id="brandtelp">
            </div>

            <div class="field">
              <label>Deskripsi Brand</label>
                <textarea placeholder="Contoh: Peralatan masak / Aksesoris handphone / Makanan siap saji" name="branddesc" id="branddesc" cols="40" rows="4" style="resize: none;"></textarea>
            </div>

            <div class="field">
              <label>Nama Perusahaan</label>
                <input type="text" placeholder="-" name="companyname2" id="companyname2">
            </div>

            <div class="field">
              <label>NPWP</label>
                <input type="text" placeholder="-" name="npwp" id="npwp">
            </div>

            <div class="field">
              <label>Foto Profil / Logo Brand</label>
              <br>
              <br>
              <img src="assets/master/img/user/add-img.png" align="left" class="ui  inverted green  medium button" onclick="document.getElementById('getBrandImg').click()" width="100px" height="90px">
              <input type='file' id="getBrandImg" style="display:none" required="required">
            </div>

            <br>
            <br>
            <br>
            <br>
            <hr style="background-color: green;">
            <br>

            <div class="field">
              <label>Facebook</label>
                <input type="text" placeholder="-" name="facebook" id="facebook">
            </div>

            <div class="field">
              <label>Instagram</label>
                <input type="text" placeholder="-" name="instagram" id="instagram">
            </div>

            <div class="field">
              <p>Informasi ini akan kami gunakan untuk pengembalian dana seperti deposit, atau refund.
              Kerahasiaan informasi dijamin.</p>
            </div>

            <div class="field">
              <label>Nama Tercantum di Rekening</label>
                <input type="text" placeholder="-" name="rekname" id="rekname">
            </div>

            <div class="field">
              <label>Nama Bank</label>
                <input type="text" placeholder="-" name="bankname" id="bankname">
            </div>

            <div class="field">
              <label>No Rekening &nbsp; &nbsp; &nbsp;</label><p>Digunakan untuk pencairan dana dan pengembalian dana</p>
                <input type="text" placeholder="-" name="rekno" id="rekno">
            </div>

            <div class="two ui buttons">
              <button class="ui  inverted green  medium button prev2">Previous</button>
              <button class="ui  inverted green  medium button submit">Submit</button>
            </div>

          </form>


        </div>

      </div>


    </div>
  </div>
</body>
@endsection

@section('rp_js')
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      var ct = 0;

      $('.next1').one('click', function(e) {

        e.preventDefault();

        $('#account').animate('slow', function() {

          if (ct > 0) {
            $('#account').removeClass('transition visible');
            $('#account').addClass('transition hidden');

          }
          $('#account').css('display', 'none');

          $('#accountS').addClass('disabled');
          $('#socialP').removeClass('disabled');
          $("#social").transition('fly right');
          $('body').css('background-color', '#e9e9e9');
          $('#social button').removeClass('inverted orange');
          $('#social button').addClass('inverted blue');
          ct++;

        });

      });

      $('.prev1').one('click', function(e) {

        e.preventDefault();
        $('#accountS').removeClass('disabled');
        $('#socialP').addClass('disabled');

        $('#social').animate('slow', function() {

          $('body').css('background-color', '#e9e9e9');
          $('#social').transition('hide');
          $("#account").transition('fly right');

        });

      });

      $('.next2').one('click', function(m) {

        m.preventDefault();

        $('#socialP').addClass('disabled');
        $('#details').removeClass('disabled');

        $('#social').animate('slow', function() {

          $('#personal button').removeClass("inverted blue");
          $('#personal button').addClass("inverted green");
          $('body').css('background-color', '#e9e9e9');
          $('#social').transition('hide');
          $('#personal').transition('fly right');
        });

      });

      $('.prev2').one('click', function(m) {

        m.preventDefault();
        $('#details').addClass('disabled');
        $('#socialP').removeClass('disabled');

        $('#personal').animate('slow', function() {

          $('body').css('background-color', '#e9e9e9');
          $('#personal').transition('hide');
          $('#social').transition('fly right');
        });

      });

      $('.submit').one('click', function(p) {

        p.preventDefault();

        $('#personal').stop();
      });

    });
  </script>
@endsection