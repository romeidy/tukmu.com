@extends('layouts.master')

@section('title', 'Profil Pengguna')

@section('up_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .jumbotron{
      border-style: solid;
      border-color: rgb(250,167,25);
      border-width: 9px;
    }
  </style>
@stop

@section('content')
  <br>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div class="jumbotron">
          <div class="row">
            <div class="col-md-4" style="text-align: center; margin-top: 30px;">
              <div class="field">
                <img src="assets/master/img/user/user-img.png" width="150px" height="150px">
                <br>
                <button style="margin-top: 10px;" class="ui  inverted orange  medium button" onclick="document.getElementById('getUserImg').click()">Unggah Foto</button>
                <input type='file' id="getUserImg" style="display:none" required="required">
              </div>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
              <h1 style="color: rgb(250,167,25);">DATA PERSONAL</h1>
              <table class="table">
                </thead>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Brand 1</td>
                  </tr>
                  <tr>
                    <td>Perusahaan</td>
                    <td>:</td>
                    <td>PT. Tenant 1</td>
                  </tr>
                  <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td>083864856534</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>Perkantoran Taman Kebon Jeruk Blok A10 no.10</td>
                  </tr>
                  <tr>
                    <td>Kelurahan</td>
                    <td>:</td>
                    <td>Kembangan</td>
                  </tr>
                  <tr>
                    <td>Kecamatan</td>
                    <td>:</td>
                    <td>Kebon Jeruk</td>
                  </tr>
                  <tr>
                    <td>Kota</td>
                    <td>:</td>
                    <td>Jakarta</td>
                  </tr>
                  <tr>
                    <td>Kode Pos</td>
                    <td>:</td>
                    <td>11620</td>
                  </tr>
                </tbody>
              </table>
              <br>
              <button class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH</b></span> <img src="assets/master/img/user/btn.png" width="20" height="20"></button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <h1 style="margin-top: 150px; color: rgb(250,167,25);">LIST BRAND</h1>
        <hr style="background-color: rgb(250,167,25);">

        <div class="jumbotron" style="border-color: rgb(244,123,80); margin-top: 50px;">
          <div class="row">
            <div class="col-md-4" style="text-align: center; margin-top: 30px;">
              <div class="field">
                <img src="assets/master/img/user/user-img.png" width="150px" height="150px">
                <br>
                <br>
                <p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
              </div>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
              <h1 style="color: rgb(250,167,25);">DATA BRAND</h1>
              <table class="table">
                </thead>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>Brand 1</td>
                  </tr>
                  <tr>
                    <td>Perusahaan</td>
                    <td>:</td>
                    <td>PT. Tenant 1</td>
                  </tr>
                  <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td>083864856534</td>
                  </tr>
                  <tr>
                    <td>NPWP</td>
                    <td>:</td>
                    <td>9999 1124 1167 1189</td>
                  </tr>
                  <tr>
                    <td>Website</td>
                    <td>:</td>
                    <td>www.brand1.com</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>Perkantoran Taman Kebon Jeruk Blok A10 no.10</td>
                  </tr>
                  <tr>
                    <td>Kelurahan</td>
                    <td>:</td>
                    <td>Kembangan</td>
                  </tr>
                  <tr>
                    <td>Kecamatan</td>
                    <td>:</td>
                    <td>Kebon Jeruk</td>
                  </tr>
                  <tr>
                    <td>Kota</td>
                    <td>:</td>
                    <td>Jakarta</td>
                  </tr>
                  <tr>
                    <td>Jenis Usaha</td>
                    <td>:</td>
                    <td>Otomotif</td>
                  </tr>
                </tbody>
              </table>
              <br>
              <button class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH</b></span> <img src="assets/master/img/user/btn.png" width="20" height="20"></button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>
  </div>
@endsection

@section('rp_js')

@endsection