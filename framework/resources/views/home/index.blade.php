@extends('layouts.master')
@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="assets/master/img/home/slider/001.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/master/img/home/slider/002.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="assets/master/img/home/slider/003.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<br>
<br>

<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="jumbotron">
				<div class="row">
				  	<div class="col-md-4">
				  		<h5>CARI</h5>
				  		<input type="text" name="search">
				  	</div>
				  	<div class="col-md-4">
				  		<h5>TANGGAL</h5>
				  		<input type="date" name="date">
				  	</div>
				  	<div class="col-md-4">
				  		<h5>KATEGORI EVENT</h5>
				  		<input type="text" name="date">
				  	</div>
				</div>
				<br><br>
				<div class="row">
				  	<div class="col-md-5"></div><!--  -->
				  	<div class="col-md-2">
				  		<button class="btn btn-defaullt" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white"><b>CARI</b></span></button>
				  	</div>
				  	<div class="col-md-5"></div>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>

<br>

<div class="container">

	<center>
		<h1>UPCOMING EVENT</h1>
	</center>
    <hr style="background-color: orange;">
	<br>
    <div class="row blog">
        <div class="col-md-12">
            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#blogCarousel" data-slide-to="1"></li>
                </ol>

                <!-- Carousel items -->
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!--.item-->

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">
                                    <img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;">
                                </a>
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!--.item-->

                </div>
                <!--.carousel-inner-->
            </div>
            <!--.Carousel-->

        </div>
    </div>
</div>
@endsection

@section('footer')
<footer class="footer">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-3 col-md col-sm-4  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">About Us</h3>
<!--headin5_amrc-->
                <p class="mb10" style="color: black;">With more than 15 years of experience we can proudly say that we are one of the best in business, a trusted supplier for more than 1000 companies.</p>


            </div>


            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Site Map</h3>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                <li><a href="#" style="color: black;">About</a></li>
                <li><a href="#" style="color: black;">Bantuan</a></li>
                <li><a href="#" style="color: black;">Daftar</a></li>
                <li><a href="#" style="color: black;">Masuk</a></li>
                </ul>
            <!--footer_ul_amrc ends here-->
            </div>

            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Get In Touch</h3>

                    <a href="#"><i class="fab fa-facebook-f" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-twitter" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-linkedin" style="margin-right: 15px;"></i></a>
                    <a href="#"><i class="fab fa-instagram" style="margin-right: 15px;"></i></a>
            </div>

            <div class=" col-sm-3 col-md  col-12 col">
                <h3 class="headin5_amrc col_white_amrc pt2">Newsletter</h3>
                <p style="color: black;">Subscribe us for new update information of our event</p>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subscribe" placeholder="Enter your email here">
                    </div>
                    <button type="submit" class="btn btn-defaullt btn-sm" style="background-color: #f47b50; color: white;">SUBSCRIBE</button>
                </form>
            </div>
        </div>
    </div>


    <div class="container">
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center" style="color: black;">Copyright @2017 | Designed With by <a href="#">tukmu.com</a></p>

        
        <!--social_footer_ul ends here-->
    </div>
</footer>
@endsection