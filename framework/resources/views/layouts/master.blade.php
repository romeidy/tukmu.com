<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta content="Web Service" name="description" />
	<meta content="Tukmu Web Service" name="author" />
	<link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
	<title>@yield('title', 'Home - Tukmu.com')</title>

	<!-- GLOBAL CSS & FONT -->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/master/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <!-- PARTIAL CSS -->
    @yield('rp_css')
    @yield('up_css')
    @yield('ep_css')


</head>
<body>

	<!-- NAVBAR SECTION -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#" style="color: white;">TUKMU.COM LOGO</a> &nbsp; &nbsp;
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="#" style="color: white;">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#" style="color: white;">Bantuan</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button type="button" class="btn btn-link" style="color: white;" data-toggle="modal" data-target="#modalregister"><img src="assets/master/img/navbar/daftar.png" width="30px" height="30px">Daftar</button>
          &nbsp; &nbsp;  
            <button type="button" class="btn btn-link" style="color: white;" data-toggle="modal" data-target="#modallogin">
          <img src="assets/master/img/navbar/masuk.png" width="30px" height="30px">Masuk</button>
        </form>
      </div>
    </nav>

    <!-- MODAL SECTION -->

    <!-- MODAL REGISTER -->
    <div class="modal fade" id="modalregister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userregister" role="tab" aria-controls="home" aria-selected="true" style="color: black;">DAFTAR PENGGUNA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminregister" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">DAFTAR PENYELENGGARA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="userregister" role="tabpanel" aria-labelledby="home-tab">
                        <center>
                            <h4>DAFTAR <strong>PENGGUNA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="repassword" placeholder="Ulangi Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">DAFTAR</button>
                                </div>
                            </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="adminregister" role="tabpanel" aria-labelledby="profile-tab">
                            <h4>APAKAH ANDA <strong>PENYELENGGARA ACARA?</strong></h4>
                            <br>
                            <p>Pastikan Anda terhubung dengan para tenan dari pasar lokan dan internasional secara cepat!</p>
                            <br>
                            <a href="#" class="btn btn-primary">DAFTAR</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL LOGIN -->
    <div class="modal fade" id="modallogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">LOGIN PENGGUNA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminlogin" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">LOGIN PENYELENGGARA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="userlogin" role="tabpanel" aria-labelledby="home-tab">
                        <center>
                            <h4>LOGIN <strong>PENGGUNA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Nama Pengguna">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">LOGIN</button>
                                </div>
                                <div class="col">
                                    <p>Lupa Kata Sandi? <a href="#modalpr" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="adminlogin" role="tabpanel" aria-labelledby="profile-tab">
                        <center>
                            <h4>LOGIN <strong>PENYELENGGARA</strong></h4>
                        </center>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Nama Penyelenggara">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">LOGIN</button>
                                </div>
                                <div class="col">
                                    <p>Lupa Kata Sandi? <a href="">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL PASSWORD RECOVERY -->
    <div class="modal fade" id="modalpr" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: orange;">
                    <ul class="nav nav-pills2" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">PEMULIHAN KATA SANDI PENGGUNA</a>
                      </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="pr" role="tabpanel" aria-labelledby="home-tab">
                        <h4>APAKAH ANDA <strong>LUPA KATA SANDI?</strong></h4>
                        <p>Silahkan masukkan email yang sudah terdaftar untuk akun anda.</p>
                        <br>
                        <br>
                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Contoh : pengguna@gmail.com">
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-warning">Kirim Link Recovery</button>
                                </div>
                                <div class="col">
                                    <p>Mengingat Kata Sandi Anda? <a href="#modallogin" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @yield('content')

    <br>
    <br>

    @yield('footer')

    <!-- GLOBAL JAVASCRIPT & JQUERY -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- PARTIAL JAVASCRIPT & JQUERY -->
    @yield('rp_js')

</body>
</html>