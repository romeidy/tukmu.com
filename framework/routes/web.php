<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('daftar-profil-pengguna', function () {
    return view('user.user_register_profile');
});

Route::get('profil-pengguna', function () {
    return view('user.user_profile');
});

Route::get('hasil-pencaharian', function () {
    return view('user.user_search_result');
});

Route::get('profil-event', function () {
    return view('event.event_profile');
});